from flask import Flask, render_template, request, flash, redirect, url_for
from owlready2 import *
import psycopg2
import psycopg2.extras

app = Flask(__name__)



app.secret_key = "brayanmvbu"

#Conexion con PostgreSQL
DB_HOST = "localhost"
DB_NAME = "db_university"
DB_USER = "postgres"
DB_PASS = "brayanmvbu"

conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)

# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU BRAYAN\\Desktop\\ontoUniversidad.owx"
# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()


def crear_tabla():
    print("Generación de Sentencias SQL para Crear Tablas:")
    for clase in onto.classes():
        print(f"\n-- Tabla para la clase: {clase.name}")
        
        # Obtener las data properties asociadas a la clase
        data_properties = [prop.name for prop in onto.data_properties() if prop.domain[0] == clase]

        # Crear la sentencia SQL
        c = f'''
            CREATE TABLE {clase.name} (
                {', '.join([f'{atributo} VARCHAR(255)' for atributo in data_properties])},
                PRIMARY KEY ({data_properties[0]})
            );
        '''
        with conn as conexion:
            with conexion.cursor() as cursor:
                cursor.execute(c)
        
        conexion.commit()
#Solo deberia ejecutarse una vez para crear las tablas usando la ontologia      

#crear_tabla()

#mostrar index.html ruta raiz
@app.route('/')
def Index():
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    s = "SELECT * FROM materia"
    cur.execute(s) # Execute the SQL
    list_users = cur.fetchall()
    return render_template('index.html', list_users = list_users)

#Insertando materias
@app.route('/add_materia', methods=['POST'])
def add_materia():
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    if request.method == 'POST':
        codmateria = request.form['codmateria']
        creditos = request.form['creditos']
        nombremateria = request.form['nombremateria']
        cur.execute("INSERT INTO materia (codmateria, creditos, nombremateria) VALUES (%s,%s,%s)", (codmateria, creditos, nombremateria))
        conn.commit()
        flash('Materia Added successfully')
        return redirect(url_for('Index'))

#Editando datos de la materia
@app.route('/edit/<codmateria>', methods = ['POST', 'GET'])
def get_employee(codmateria):
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
   
    cur.execute('SELECT * FROM materia WHERE codmateria = %s', (codmateria))
    data = cur.fetchall()
    cur.close()
    print(data[0])
    return render_template('edit.html', materia = data[0])

#Actualizando datos de la materia
@app.route('/update/<codmateria>', methods=['POST'])
def update_materia(codmateria):
    if request.method == 'POST':
        codmateria = request.form['codmateria']
        creditos = request.form['creditos']
        nombremateria = request.form['nombremateria']
         
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("""
            UPDATE materia
            SET creditos = %s,
                nombremateria = %s
            WHERE codmateria = %s
        """, (creditos, nombremateria, codmateria))
        flash('Materia Updated Successfully')
        conn.commit()
        return redirect(url_for('Index'))
    
#Eliminando un Materia
@app.route('/delete/<string:codmateria>', methods = ['POST','GET'])
def delete_materia(codmateria):
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
   
    cur.execute('DELETE FROM materia WHERE codmateria = {0}'.format(codmateria))
    conn.commit()
    flash('Materia Removed Successfully')
    return redirect(url_for('Index'))

if __name__ == '__main__':
    app.run(debug=True, port=5000)
